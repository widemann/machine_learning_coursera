function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda, varargin)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%   varargin{1} = 'L1', or don't use it to default to L2.

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

X = [ones(m,1) X];
h_t1 = [ones(1,m); sigmoid(Theta1*(X'))];
h_t2 = sigmoid(Theta2*h_t1);
% Y = eye(num_labels);
% tic; % there is a lot of multiplication by zero below. 
% for i = 1:m
%     for j = 1:num_labels
%         J = J - (Y(j,y(i))*log(h_t2(j,i)) + (1 - Y(j,y(i)))*log(1-h_t2(j,i)));
%     end
% end
% J = (1/m)*J;
% toc

% J = 0;
% tic; 
for i = 1:m
    for j = 1:num_labels
        if j == y(i)
            J = J - log(h_t2(j,i));
        else
            J = J - log(1-h_t2(j,i));
        end
    end
end
J = (1/m)*J;
% toc

% toc
% After the initial run, the above is faster than the vectorized version
% below. Is this the JIT compiler in action???
% tic;
% J2 = 0;
% for i = 1:m
%     J2 = J2 - (Y(y(i),:)*log(h_t2(:,i)) + (1 - Y(y(i),:))*log(1-h_t2(:,i)));
% end
% J2 = (1/m)*J2;
% toc

if nargin > 7 % maybe do L1 regularization
    if strcmpi(varargin{1},'L1')
        regTerm = (lambda/(2*m))*(sum(sum(abs(Theta1(:,2:end)))) + sum(sum(abs(Theta2(:,2:end)))));
    end
else % do the normal L2 regularization.
    regTerm = (lambda/(2*m))*(sum(sum(Theta1(:,2:end).^2)) + sum(sum(Theta2(:,2:end).^2)));
end
J = J + regTerm;


% -------------------------------------------------------------
%% do the backpropagation
Y = eye(num_labels);
% D1 = zeros(hidden_layer_size, input_layer_size);
% D2 = zeros(num_labels, hidden_layer_size);
D1 = zeros(size(Theta1));
D2 = zeros(size(Theta2));
for trainIdx = 1:m
   % put steps 1-4 here
   
   % step 1
   z2 = Theta1*(X(trainIdx,:)');
   a2 = [1; sigmoid(z2)];
   z3 = Theta2*(a2);
   a3 = sigmoid(z3);
   
   % step 2
   delta3 = a3 - Y(:,y(trainIdx));
   
   % step 3
   delta2 = (Theta2(:,2:end)'*delta3).*sigmoidGradient(z2);
   
   % step 4
%    D1 = D1 + delta2*(X(trainIdx,2:end));
%    D2 = D2 + delta3*(a2(2:end)');
   D1 = D1 + delta2*(X(trainIdx,:));
   D2 = D2 + delta3*(a2');
   
end

Theta1_grad = (1/m)*D1;
Theta1_grad(:,2:end) = Theta1_grad(:,2:end) + (lambda/m)*Theta1(:,2:end);

Theta2_grad = (1/m)*D2;
Theta2_grad(:,2:end) = Theta2_grad(:,2:end) + (lambda/m)*Theta2(:,2:end); 


% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
