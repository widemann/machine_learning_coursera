These are the homework solutions to Professor Ng's coursera machine learning class. Each folder contains 
a problem set for that week. In each folder there is a pdf file that describes the type of algorithm being 
studied. 

week 1: Linear regression
week 2: Logistic regression
week 3: Multi-class & neural networks
week 4: Neural networks
week 5: Regularization
week 6: Support vector machines
week 7: K-means and PCA
week 8: Anomaly detection and recommender systems